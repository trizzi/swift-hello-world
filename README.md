# Swift Hello World

Hello World web server in Swift.

```
$ docker run -d -p 8080:8080 registry.gitlab.com/jdrpereira/swift-hello-world:latest
```

```
$ curl localhost:8080
Hello world
```