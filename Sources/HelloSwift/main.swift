import Swifter
import Dispatch

let server = HttpServer()

server["/"] = { r in
    return HttpResponse.ok(.html("Hello world"))
}

let semaphore = DispatchSemaphore(value: 0)

do {
    try server.start(8080, forceIPv4: true)
    print("Server listening on port \(try server.port())")
    semaphore.wait()
} catch {
    print("Server start error: \(error)")
    semaphore.signal()
}
