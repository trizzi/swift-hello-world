// swift-tools-version:5.0

import PackageDescription

let package = Package(
    name: "app",
    dependencies: [
        .package(url: "https://github.com/httpswift/swifter.git", .upToNextMajor(from: "1.4.7"))
    ],
    targets: [
        .target(
        name: "app",
        dependencies: ["Swifter"],
        path: "./Sources")
    ]
)
